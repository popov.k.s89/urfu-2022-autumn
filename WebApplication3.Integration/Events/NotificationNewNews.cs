namespace WebApplication3.Integration.Events;

public interface NotificationNewNews
{
    long NewsId { get; set; }
}