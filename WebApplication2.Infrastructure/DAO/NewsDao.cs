using WebApplication2.Core.DataStorage.DAO;
using WebApplication2.Core.Model;

namespace WebApplication2.Infrastructure.DAO;

public sealed class NewsDao : INewsDao
{
    public Task<News> GetNewsById(long id)
    {
        throw new NotImplementedException();
    }

    public Task<News> Add(News news)
    {
        throw new NotImplementedException();
    }

    public Task RemoveNewsById(long id)
    {
        throw new NotImplementedException();
    }

    public Task RemoveNewsByTitle(string title)
    {
        throw new NotImplementedException();
    }

    public Task<IReadOnlyList<News>> SearchNewsByCreateDate(DateTime createDate)
    {
        throw new NotImplementedException();
    }

    public Task<IReadOnlyList<News>> SearchNewsByTitle(string title)
    {
        throw new NotImplementedException();
    }

    public Task<IReadOnlyList<News>> SearchNewsByCreateDateAndTitle(DateTime createDate, string title)
    {
        throw new NotImplementedException();
    }

    public Task<News> Update(News news)
    {
        throw new NotImplementedException();
    }
}