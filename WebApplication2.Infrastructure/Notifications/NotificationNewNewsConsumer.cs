using System.Diagnostics;
using MassTransit;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication3.Integration.Events;

namespace WebApplication2.Infrastructure.Notifications;
 
public class NotificationNewNewsConsumer : IConsumer<NotificationNewNews>, IConsumer<Fault<NotificationNewNews>>
{
    private readonly INewsRepository _newsRepository;

    public NotificationNewNewsConsumer(INewsRepository newsRepository)
    {
        _newsRepository = newsRepository;
    }
    
    public async Task Consume(ConsumeContext<NotificationNewNews> context)
    {
         var news = await _newsRepository.FindAsync(context.Message.NewsId, context.CancellationToken);

        Debug.WriteLine($"Уведомление о добавление новой новости : {news.Title}");
    }

    public Task Consume(ConsumeContext<Fault<NotificationNewNews>> context)
    {
        throw new NotImplementedException();
    }
}