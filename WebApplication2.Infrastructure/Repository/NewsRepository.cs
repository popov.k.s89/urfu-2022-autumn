using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.Model;
using WebApplication2.Core.SharedKernel.Specifications;

namespace WebApplication2.Infrastructure.Repository;

public sealed class NewsRepository : EFRepository<News>, INewsRepository
{
    private readonly WebApplicationDbContext _unitOfWork;

    public NewsRepository(WebApplicationDbContext unitOfWork) : base(unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    //todo: когда модель хранения отличается от доменной модели
    // public override Task<News> AddAsync(News entity, CancellationToken ct)
    // {
    //     var newsEntity = new NewsEntity
    //     {
    //         JsonValue = JsonSerializer.Serialize(entity)
    //     };
    //
    //     var entry = _unitOfWork.News.Add(newsEntity);
    //     entity.Id = entry.Entity.Id;
    //     
    //     return Task.FromResult(entity);
    // }

    public Task<IReadOnlyList<News>> VeryFastSearchNews()
    {
        throw new NotImplementedException();
    }
    
}