using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.Model;

namespace WebApplication2.Infrastructure.Repository;

public sealed class AuthorRepository : EFRepository<Author>, IAuthorRepository
{
    public AuthorRepository(WebApplicationDbContext unitOfWork) : base(unitOfWork)
    {
     
    }
    
    //todo: когда модель хранения отличается от доменной модели
    // public override Task<News> AddAsync(News entity, CancellationToken ct)
    // {
    //     var newsEntity = new NewsEntity
    //     {
    //         JsonValue = JsonSerializer.Serialize(entity)
    //     };
    //
    //     var entry = _unitOfWork.News.Add(newsEntity);
    //     entity.Id = entry.Entity.Id;
    //     
    //     return Task.FromResult(entity);
    // }
    
}