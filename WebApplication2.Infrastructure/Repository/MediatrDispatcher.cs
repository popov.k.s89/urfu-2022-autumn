using MediatR;
using WebApplication2.Core.SharedKernel;

namespace WebApplication2.Infrastructure.Repository
{
    public static class MediatrDispatcher
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, WebApplicationDbContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.Entity.ClearDomainEvents());

            var tasks = domainEvents
                .Select(async (domainEvent) => {
                    await mediator.Publish(domainEvent);
                }).ToArray();

            await Task.WhenAll(tasks);
        }    
    }
}