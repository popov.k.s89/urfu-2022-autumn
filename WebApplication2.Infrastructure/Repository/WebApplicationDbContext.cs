using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using WebApplication2.Core.Model;
using WebApplication2.Core.SharedKernel.Repository;

namespace WebApplication2.Infrastructure.Repository;

public class WebApplicationDbContext : DbContext, IUnitOfWork
{

    static WebApplicationDbContext()
    {
       //Npgsql.Le
    }
    public WebApplicationDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
    {
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(WebApplicationDbContext).Assembly);
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        var mediatr = this.GetService<IMediator>();
        await mediatr.DispatchDomainEventsAsync(this);
        
        var i = await base.SaveChangesAsync(cancellationToken);

        return i;
    }

    public DbSet<News> News { get; set; }
    public DbSet<Author> Authors { get; set; }
}