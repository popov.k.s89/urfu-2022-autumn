using System.Linq.Expressions;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebApplication2.Core.Model;

namespace WebApplication2.Infrastructure.Repository.EntityConfiguration;

public class NewsEntityConfiguration : IEntityTypeConfiguration<News>
{
    public void Configure(EntityTypeBuilder<News> builder)
    {
        builder.OwnsOne(x => x.Address, c =>
        {
            c.Property(a => a.City).HasMaxLength(10);
        });

        // //в реальных проектах конфигурация должна использоваться общая или 
        // //задаваться один раз специально для объектов хранения
        // var jsonSerializer = new JsonSerializerOptions();
        //
        // builder.Property(x => x.Address)
        //     .HasConversion(x => JsonSerializer.Serialize(x, jsonSerializer),
        //         x => JsonSerializer.Deserialize<Address>(x, jsonSerializer));
    }
}