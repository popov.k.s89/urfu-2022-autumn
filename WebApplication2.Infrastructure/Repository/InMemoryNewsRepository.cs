using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.Model;
using WebApplication2.Core.SharedKernel.Repository;
using WebApplication2.Core.SharedKernel.Specifications;

namespace WebApplication2.Infrastructure.Repository;

public class InMemoryNewsRepository : INewsRepository
{
    private readonly Dictionary<long, News> _items = new();
    public bool ReadOnly { get; }
    public Task<News> FindAsync(long id, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News[]> ListAsync(CancellationToken cancellationToken)
    {
        return Task.FromResult(_items.Values.ToArray());
    }

    public Task<News[]> ListAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        return Task.FromResult(_items.Values.Where(specification).ToArray());
    }

    public Task<News> SingleAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> SingleAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> SingleOrDefaultAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> SingleOrDefaultAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> FirstAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> FirstAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> FirstOrDefaultAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> FirstOrDefaultAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<int> CountAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<int> CountAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<long> LongCountAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<long> LongCountAsync(ISpecification<News> specification, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<TResult[]> Query<TResult>(Func<IQueryable<News>, IQueryable<TResult>> query, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task<News> AddAsync(News entity, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task AddRangeAsync(IEnumerable<News> entities, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public Task RemoveAsync(News entity)
    {
        throw new NotImplementedException();
    }

    public Task RemoveRangeAsync(IEnumerable<News> entities)
    {
        throw new NotImplementedException();
    }

    public IUnitOfWork UnitOfWork { get; }
    public Task<IReadOnlyList<News>> VeryFastSearchNews()
    {
        throw new NotImplementedException();
    }
}