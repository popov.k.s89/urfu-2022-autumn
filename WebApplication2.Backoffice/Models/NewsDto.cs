namespace WebApplication2.Backoffice.Models;

public sealed class NewsDto
{
    public long? Id { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
    public string Image { get; set; }
    public DateTime? CreateAt { get; set; }
    public long AuthorId { get; set; }
    public string Address { get; set; }
}