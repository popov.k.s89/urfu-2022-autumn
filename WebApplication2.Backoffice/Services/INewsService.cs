using WebApplication2.Backoffice.Models;

namespace WebApplication2.Backoffice.Services;

public interface INewsService
{
    Task<NewsDto> CreateNewsAsync(NewsDto newsDto, CancellationToken cancellationToken);
    Task<IReadOnlyList<NewsDto>> SearchAsync(DateTime? createDate, string title, CancellationToken cancellationToken);
}