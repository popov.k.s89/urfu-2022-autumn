using WebApplication2.Backoffice.Models;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.Model;
using WebApplication2.Core.SharedKernel.Specifications;

namespace WebApplication2.Backoffice.Services;

public sealed class NewsService : INewsService
{
    private readonly INewsRepository _newsRepository;
    public NewsService(INewsRepository newsRepository)
    {
        _newsRepository = newsRepository;
    }
    
    public async Task<NewsDto> CreateNewsAsync(NewsDto newsDto, CancellationToken cancellationToken)
    {
        News news;
        if (newsDto.Id.HasValue)
        {
            news = await _newsRepository.FindAsync(newsDto.Id.Value, cancellationToken);
            
            news.Image = newsDto.Image;
            news.Title = newsDto.Title;
            news.Text = newsDto.Text;
        }
        else
        {
            var address = new Address("City", "Address1");
            news = new News(address)
            {
                CreateAt = DateTime.Now,
                Image = String.Empty,
                Text = "Test news",
                Title = "Test news title"
            };
            
            await _newsRepository.AddAsync(news, cancellationToken);
        }
        
        await _newsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
        return Convert(news);
    }
    
    public async Task<IReadOnlyList<NewsDto>> SearchAsync(DateTime? createDate, string title, CancellationToken cancellationToken)
    {
        var searchFilterSpecification = Specification<News>.Empty();
        
        if (createDate.HasValue)
        {
            searchFilterSpecification = searchFilterSpecification
                .And(NewsSpecifications.FilterByDate(createDate.Value));
        }

        if (!String.IsNullOrWhiteSpace(title))
        {
            searchFilterSpecification = searchFilterSpecification
                .And(NewsSpecifications.FilterByTitle(title));
        }

        var foundNews = await _newsRepository.ListAsync(searchFilterSpecification, cancellationToken);
        return foundNews.Select(Convert).ToList();
    }
    
    NewsDto Convert(News news) => new()
    {
        CreateAt = news.CreateAt,
        Id = news.Id,
        Title = news.Title,
        Image = news.Image,
        Text = news.Text
    };
}
