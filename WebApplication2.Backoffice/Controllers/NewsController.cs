﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using WebApplication2.Backoffice.Features.News;
using WebApplication2.Backoffice.Models;
using WebApplication2.Backoffice.Services;

namespace WebApplication2.Backoffice.Controllers
{
    public class NewsController : BackofficeBaseController
    {
        private readonly INewsService _newsService;
        private readonly IMediator _mediator;
        private readonly IMemoryCache _memoryCache;
        private readonly IDistributedCache _distributedCache;

        public NewsController(INewsService newsService,
            IMediator mediator,
            IMemoryCache memoryCache,
            IDistributedCache distributedCache)
        {
            _newsService = newsService;
            _mediator = mediator;
            _memoryCache = memoryCache;
            _distributedCache = distributedCache;
        }

        [HttpGet("/api/news/searchNews")]
        public async Task<IActionResult> SearchNews([FromQuery]DateTime? createDate,
            [FromQuery]string title, CancellationToken cancellationToken)
        {
            var cacheEntry = _memoryCache.CreateEntry("1");
            cacheEntry.SetValue(new NewsDto());

            _memoryCache.GetOrCreate("1", entry => new NewsDto());
            
            await _memoryCache.GetOrCreateAsync("1", entry => Task.FromResult(new NewsDto()));
            await _memoryCache.GetOrCreateAsync("1", entry =>  _newsService.SearchAsync(createDate, title, cancellationToken));
            
            
            
            var foundNews = await _newsService.SearchAsync(createDate, title, cancellationToken);
            if (foundNews.Count == 0)
            {
                return NotFound();
            }
            
            //await _mediator.Send(command, this.HttpContext.RequestAborted);
            return Ok(foundNews);
        }
        
        [HttpGet("/api/news/searchNews2")]
        public async Task<IActionResult> SearchNews2(NewsSearchQuery query,
            CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(query, cancellationToken);
            if (!result.IsSuccessfull)
            {
                return BadRequest(result.GetErrors());
            }
            
            if (result.Value.Count == 0)
            {
                return NotFound();
            }
            
            return Ok(result.Value);
        }
        
        [HttpPost("/api/news/createNews")]
        public async Task<IActionResult> CreateNews(NewsCreateCommand command,
            CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            if (!result.IsSuccessfull)
            {
                return BadRequest(result.GetErrors());
            }
            
            return Ok();
        }
        
    }
    
    
    
}