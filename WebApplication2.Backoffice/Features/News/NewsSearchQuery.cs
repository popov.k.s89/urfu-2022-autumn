using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Backoffice.Models;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.Model;
using WebApplication2.Core.SharedKernel.CQS;
using WebApplication2.Core.SharedKernel.Result;
using WebApplication2.Core.SharedKernel.Specifications;
using WebApplication2.Infrastructure.Repository;

namespace WebApplication2.Backoffice.Features.News;

public sealed class NewsSearchQuery: Query<IReadOnlyList<NewsDto>>
{
    [FromQuery]
    public DateTime? CreateDate { get; set; }
    [FromQuery]
    public string Title { get; set; }
}

public sealed class NewsSearchQueryHandler : QueryHandler<NewsSearchQuery, IReadOnlyList<NewsDto>>
{
    private readonly INewsRepository _newsRepository;

    public NewsSearchQueryHandler(INewsRepository newsRepository)
    {
        _newsRepository = newsRepository;
    }
    
    public override async Task<Result<IReadOnlyList<NewsDto>>> Handle(NewsSearchQuery request, CancellationToken cancellationToken)
    {
        var searchFilterSpecification = Specification<Core.Model.News>.Empty();
        
        if (request.CreateDate.HasValue)
        {
            searchFilterSpecification = searchFilterSpecification
                .And(NewsSpecifications.FilterByDate(request.CreateDate.Value));
        }

        if (!String.IsNullOrWhiteSpace(request.Title))
        {
            searchFilterSpecification = searchFilterSpecification
                .And(NewsSpecifications.FilterByTitle(request.Title));
        }

        try
        {
            var foundNews = await _newsRepository.ListAsync(searchFilterSpecification, cancellationToken);

            var firstNews = foundNews.First();
            var address2 = new Address("City", "Address2");
            var b1 = firstNews.Address == address2;
            var b2 = firstNews.Address.Equals(address2);
            
            
            return Successfull(foundNews.Select(NewsConvertor.Convert).ToList());
        }
        catch (Exception e)
        {
            return Error(new TestError("NotImplementedException"));
        }
    }
}

public sealed class TestError : Error
{
    public TestError(string errorText)
    {
        Data[nameof(errorText)] = errorText;
    }

    public override string Type => "webapplication.testerror";
}