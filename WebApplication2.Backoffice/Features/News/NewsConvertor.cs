using WebApplication2.Backoffice.Models;

namespace WebApplication2.Backoffice.Features.News;

public static class NewsConvertor
{
    public static NewsDto Convert(Core.Model.News news) => new()
    {
        CreateAt = news.CreateAt,
        Id = news.Id,
        Title = news.Title,
        Image = news.Image,
        Text = news.Text,
        AuthorId = news.AuthorId,
        Address = news.Address.ToString()
    };
}