using WebApplication2.Backoffice.Models;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.SharedKernel.CQS;
using WebApplication2.Core.SharedKernel.Result;

namespace WebApplication2.Backoffice.Features.News;

public sealed class NewsUpdateCommand : Command
{
    public NewsDto NewsDto { get; set; }
}

public sealed class NewsUpdateCommandHandler : CommandHandler<NewsUpdateCommand>
{
    private readonly INewsRepository _newsRepository;

    public NewsUpdateCommandHandler(INewsRepository newsRepository)
    {
        _newsRepository = newsRepository;
    }
    public override async Task<Result> Handle(NewsUpdateCommand request, CancellationToken cancellationToken)
    {
        var news = await _newsRepository.FindAsync(request.NewsDto.Id.Value, cancellationToken);
            
        news.Image = request.NewsDto.Image;
        news.Title = request.NewsDto.Title;
        news.Text = request.NewsDto.Text;

        await _newsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
        return Successfull();
    }
}