using MassTransit;
using MediatR;
using WebApplication2.Backoffice.Models;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.Model;
using WebApplication2.Core.Model.Events;
using WebApplication2.Core.SharedKernel.CQS;
using WebApplication2.Core.SharedKernel.Result;
using WebApplication3.Integration.Events;

namespace WebApplication2.Backoffice.Features.News;

public sealed class NewsCreateCommand : Command
{
    public NewsDto NewsDto { get; set; }
}

public sealed class NewsCreateCommandHandler : CommandHandler<NewsCreateCommand>
{
    private readonly INewsRepository _newsRepository;
    private readonly IBus _bus;

    public NewsCreateCommandHandler(INewsRepository newsRepository, IBus bus)
    {
        _newsRepository = newsRepository;
        _bus = bus;
    }
    
    public override async Task<Result> Handle(NewsCreateCommand request, CancellationToken cancellationToken)
    {
        var address = new Address("City", "Address1");   
        
        var news = new Core.Model.News(address)
        {
            CreateAt = DateTime.UtcNow,
            Image = String.Empty,
            Text = request.NewsDto.Text,
            Title = request.NewsDto.Title,
            AuthorId = request.NewsDto.AuthorId,
        };
        
        news.AddEvent(new AddNewsEvent()
        {
            News = news
        });
            
        await _newsRepository.AddAsync(news, cancellationToken);
        await _newsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
        
                
        await _bus.Publish<NotificationNewNews>(new
        {
            NewsId = news.Id
        }, cancellationToken);
        
        return Successfull();
    }
}