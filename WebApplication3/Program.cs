using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using WebApplication2.Backoffice.Controllers;
using WebApplication2.Backoffice.Services;
using WebApplication2.Core.DataStorage.DAO;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.SharedKernel;
using WebApplication2.Infrastructure.DAO;
using WebApplication2.Infrastructure.Notifications;
using WebApplication2.Infrastructure.Repository;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddScoped<INewsDao, NewsDao>();
builder.Services.AddScoped<INewsService, NewsService>();
builder.Services.AddScoped<INewsRepository, NewsRepository>();
builder.Services.AddScoped<IAuthorRepository, AuthorRepository>();

builder.Services.AddSwaggerGen(c =>  {
    c.CustomSchemaIds(x => x.FullName);
    c.SwaggerDoc("v1", new OpenApiInfo {Title = "API", Version = "v1"});
});
builder.Services.AddControllers();

builder.Services.AddMemoryCache();
builder.Services.AddDistributedMemoryCache();

builder.Services.AddMediatR(typeof(BackofficeBaseController), typeof(IDomainEvent));

builder.Services.AddDbContext<WebApplicationDbContext>(optionsBuilder =>
{
    var dbConnection = builder.Configuration.GetConnectionString("WebApplication");
    optionsBuilder.UseNpgsql(dbConnection);
});

builder.Services.AddMassTransit(x =>
            {
                
                x.AddConsumer<NotificationNewNewsConsumer>();
                
                x.UsingRabbitMq((context, configurator) =>
                {
                    configurator.Host(builder.Configuration.GetConnectionString("RabbitMq"));
                    
                    configurator.ConfigureEndpoints(context);
                });
                
                
                // x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                // {
                //     cfg.Host(builder.Configuration.GetConnectionString("RabbitMq"));
                //
                //     cfg.ReceiveEndpoint("Events", c =>
                //     {
                //         c.ConfigureConsumer<NotificationNewNewsConsumer>(provider);
                //     });
                // }));
            });



var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1"));
app.UseDeveloperExceptionPage();

app.UseRouting();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});






app.Run();