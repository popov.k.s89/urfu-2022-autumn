using WebApplication2.Core.SharedKernel;

namespace WebApplication2.Core.Model;

public class Author : BaseModel, IAggregateRoot
{
    public string Name { get; set; }
    public long NewsCount { get; set; }
}