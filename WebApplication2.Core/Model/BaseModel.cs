using WebApplication2.Core.SharedKernel;

namespace WebApplication2.Core.Model;

public class BaseModel : Entity
{
    public long Id { get; set; }
}