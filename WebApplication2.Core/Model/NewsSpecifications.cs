using WebApplication2.Core.SharedKernel.Specifications;

namespace WebApplication2.Core.Model;

public static class NewsSpecifications
{
    public static ISpecification<News> FilterByDate(DateTime createDate) =>
        Specification<News>.Create(x => x.CreateAt > createDate);
    
    public static ISpecification<News> FilterByTitle(string title) => 
        Specification<News>.Create(x => x.Title.Contains(title));
}