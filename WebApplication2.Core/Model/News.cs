using WebApplication2.Core.SharedKernel;

namespace WebApplication2.Core.Model
{
    public class News : BaseModel, IAggregateRoot 
    {
        private News()
        {
            
        }
        public News(Address address)
        {
            Address = address;
        }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public DateTime CreateAt { get; set; }
        public Address Address { get; set; }
        public long AuthorId { get; set; }
    }
}