using MediatR;
using WebApplication2.Core.DataStorage.Repositories;
using WebApplication2.Core.SharedKernel;

namespace WebApplication2.Core.Model.Events;

public class AddNewsEvent : IDomainEvent
{
    public News News { get; set; }
}

public sealed class AddNewsEventHandler : INotificationHandler<AddNewsEvent>
{
    private readonly IAuthorRepository _authorRepository;

    public AddNewsEventHandler(IAuthorRepository authorRepository)
    {
        _authorRepository = authorRepository;
    }
    
    public async Task Handle(AddNewsEvent notification, CancellationToken cancellationToken)
    {
        var author = await _authorRepository.FindAsync(notification.News.AuthorId, cancellationToken);
        author.NewsCount++;
    }
}