using WebApplication2.Core.SharedKernel;
using WebApplication2.Core.SharedKernel.Extensions;

namespace WebApplication2.Core.Model;

public sealed class Address: ValueObject
{
    private Address()
    {
        
    }
    public Address(string city, string address1)
    {
        City = city;
        Address1 = address1;
    }
    public string City { get; private set; }
    public string Address1 { get; private set; }
    
    //public IReadOnlyList<string> Values { get; private set; }
    protected override IEnumerable<object> GetAtomicValues()
    {
        yield return City;
        yield return Address1;
        //yield return EnumerableExtensions.GetHashCode(Values);
    }

    public override string ToString() => $"{City}, {Address1}";
}