using System.Collections.Concurrent;
using Microsoft.Extensions.Caching.Memory;

namespace WebApplication2.Core;

public interface ICacheService
{
     void Remove(string key);
     Task<TItem> GetOrCreateAsync<TItem>(object key, Func<ICacheEntry, Task<TItem>> factory);
}

public class CacheService : ICacheService
{
     private readonly IMemoryCache _memoryCache;

     private readonly ConcurrentDictionary<object, SemaphoreSlim> _locks = new();

     public CacheService(IMemoryCache memoryCache)
     {
          _memoryCache = memoryCache;
     }
     
     public void Remove(string key)
     {
          throw new NotImplementedException();
     }

     public async Task<TItem> GetOrCreateAsync<TItem>(object key, Func<ICacheEntry, Task<TItem>> factory)
     {
          if (_memoryCache.TryGetValue(key, out TItem item))
          {
               return item;
          }
          
          var lockKey = new { key };
          if (!_locks.TryGetValue(lockKey, out var semaphoreSlim))
          {
               semaphoreSlim = new SemaphoreSlim(1);
               _locks.TryAdd(lockKey, semaphoreSlim);
          }
          
          try
          {
               await semaphoreSlim.WaitAsync();
               
               if (_memoryCache.TryGetValue(key, out item))
               {
                    return item;
               }

               item = await _memoryCache.GetOrCreateAsync(key, factory);
          }
          catch (Exception e)
          {
               //something
          }
          finally
          {
               semaphoreSlim.Release();
          }

          return item;
     }
}