﻿namespace WebApplication2.Core.SharedKernel
{
    public interface IKey<TEntity>
    {
        TEntity New();
    }
}