using MediatR;
using WebApplication2.Core.SharedKernel.Result;

namespace WebApplication2.Core.SharedKernel.CQS
{
    public class Query<TResult>: IQuery<Result<TResult>>, IRequest<Result<TResult>>
    {
        
    }
}