using WebApplication2.Core.Model;
using WebApplication2.Core.SharedKernel.Repository;

namespace WebApplication2.Core.DataStorage.Repositories
{
    public interface INewsRepository : IRepository<News>
    {
        Task<IReadOnlyList<News>> VeryFastSearchNews();
    }
}