using WebApplication2.Core.Model;

namespace WebApplication2.Core.DataStorage.DAO;

public interface INewsDao
{
    Task<News> GetNewsById(long id);

    Task<News> Add(News news);

    Task RemoveNewsById(long id);
    Task RemoveNewsByTitle(string title);

    Task<IReadOnlyList<News>> SearchNewsByCreateDate(DateTime createDate);
    Task<IReadOnlyList<News>> SearchNewsByTitle(string title);
    //есть возможность для проектирования 
    Task<IReadOnlyList<News>> SearchNewsByCreateDateAndTitle(DateTime createDate, string title);
    Task<News> Update(News news);
}